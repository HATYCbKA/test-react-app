module.exports = {
    "env": {
        "browser": true,
        "es2021": true
    },
    "extends": [
        "eslint:recommended",
        "plugin:react/recommended"
    ],
    "parserOptions": {
        "ecmaFeatures": {
            "jsx": true
        },
        "ecmaVersion": 12,
        "sourceType": "module"
    },
    "plugins": [
        "react"
    ],
    "rules": {
	    "semi": ["error", "always"],
	    "quotes": ["warn", "double"],
	    "react/jsx-uses-react": "error",
            "react/jsx-uses-vars": "error",
	    "react/react-in-jsx-scope": "warn",
	    "no-mixed-spaces-and-tabs": "warn",
	    "no-undef": "warn"

    }
};
